import React from 'react';
import styles from './files-list.styles.css';
import { getFiles } from './files.resource.js';
import TableHeaders from './table-headers.component.js';
import Folder from './folder.component.js';

export default class FilesList extends React.Component {
  state = {
    folders: null,
  };
  
  componentDidMount() {
    getFiles()
      .subscribe(
        folders => this.setState({ folders }),
        err => { throw err; }
      );
  }
  
  render() {
    return (
      <div className={styles.table}>
        <h1>
          Ye olde files
        </h1>
        <TableHeaders />
        {this.state.folders &&
          this.state.folders.map(folder => (
            <Folder 
              key={folder.id} 
              folder={folder} 
              indentationLevel={1} 
            />
          ))
        }
      </div>
    );
  }
}
