# What the code in this project does

This is a React project which renders a file tree. The tree contains folders where each folder may contain files and/or additional folders. For each folder and file, we display the name, the date it was uploaded, and the person who uploaded it.

That's it! It doesn't actually do file uploading or downloading. Nor does it even let you expand or collapse folders

# Instructions for interviewee

Look over this [pull request](https://gitlab.com/CanopyTax/frontend-interview-pull-request/merge_requests/1) before coming into the interview. There are more instructions there.
